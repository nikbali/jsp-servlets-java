
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
	  <%@ page contentType="text/html;charset=utf-8" %>
	  <title>German Testing</title>
	  <style><%@include file="/style/main_style.css"%></style>
	  <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet">
	 
 </head>
 <body>
 
  <div id="container">
	   <div id="header">Testing for citizenship</div>
	   
	   <div id="sidebar"> 
			<p><a href="jsp/test_process.jsp" class="btn_menu">Start the test</a></p>
			<p><a href="jsp/create.jsp"class="btn_menu">Add question</a></p>
			<p><a href="https://github.com/nikbali/"class="btn_menu">Update question</a></p>
	   </div>
	   
	   <div id="content">
			<h2>Einbürgerungsbehörde</h2>
			<ul>
					 <li>Examination, which every foreigner must pass in order to qualify for German citizenship. It is a test of 33 questions, 4 answers each, one of which is correct. To get a positive result, it's enough to answer at least 17 correctly.</li>
					 <li>The test catalog contains 300 variants of questions that are the same throughout Germany, as well as 10 additional questions for each German land. Thus, in each case 33 questions will be randomly selected from 310 options.</li>
					 <li>The test is simple. Most questions can be answered without special preparation. It is enough to read articles on this site, and the chances of a positive final result will be quite real. According to statistics, 3% of applicants for German citizenship do not take Einb r rgerungstest from the first time.</li>
			</ul> 
	   </div>
	   
	   
	   <div id="footer">&copy; Nikita Balily</div>
  </div> 
 </body>
</html>
